var cool = require('cool-ascii-faces');
var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var dboper = require('./operations');

var mongoose = require('mongoose');
var Projects = require ('./projects');

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.set('view engine', 'html');

app.get('/', function(request, response) {
    response.sendfile('./public/index.html');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

app.get('/cool', function(request, response) {
  response.send(cool());
});


//mongo setup
// Connection URL
var url = 'mongodb://couturelp:24balanceFocused@ds021771.mlab.com:21771/heroku_51snb6v4';
// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
    assert.equal(err,null);
    console.log("Connected correctly to MongoDB server");
    
    dboper.insertDocument(db, { title: "TitleOne", url: "www.one.com" },
        "projects", function (result) {
            console.log(result.ops);
            dboper.findDocuments(db, "projects", function (docs) {
                console.log(docs);
                dboper.updateDocument(db, { title: "TitleOne" },
                    { url: "www.onemodif.com" },
                    "projects", function (result) {
                        //console.log(result.result);
                        console.log("updateDocs done 51");
                        dboper.findDocuments(db, "projects", function (docs) {
                            //console.log(docs);
                            console.log("findDocs done 54");
                            db.dropCollection("projects", function (result) {
                                console.log("drop Done 56");
                                //console.log(result);
                                console.log("***first cycle done***)")
                                db.close();
                            });
                        });
                    });
            });
        });    
});

MongoClient.connect(url, function (err, db) {
       dboper.insertDocument(db, { title: "TitleTwo", url: "www.two.com" },
        "url", function (result) {
            console.log(result.ops);
            dboper.findDocuments(db, "url", function (docs) {
                console.log(docs);
                dboper.updateDocument(db, { title: "TitleOne" },
                    { url: "www.onemodif.com" },
                    "url", function (result) {
                        //console.log(result.result);
                        console.log("updateDocs done 51");
                        dboper.findDocuments(db, "url", function (docs) {
                            //console.log(docs);
                            console.log("findDocs done 54");
                            db.dropCollection("url", function (result) {
                                console.log("drop Done 56");
                                //console.log(result);
                                console.log("***first2 cycle done***)")
                                db.close();
                            });
                        });
                    });
            });
        });
});





