// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// create a schema
var projectsSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    contacts: {
        type : Array , "default" : [] 
    },
    sites: {
        type : Array , "default" : [] 
    },
    hostingSelect:{
        type: Boolean,
        required: false
    },
    hosting: {
        type: String,
        required: false
    },
    comments: {
        type: String,
        required: false
    }    
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Projects = mongoose.model('Project', projectsSchema);

// make this available to our Node applications
module.exports = Projects;
console.log("Projects Mongoose Schema LOADED");