

// code style: https://github.com/johnpapa/angular-styleguide 
(function () {
    'use strict';
    angular
        .module('app') 
        .controller('ProjectCtrl', ['$scope', '$filter', '$http', 'editableOptions', 'editableThemes', 'projFactory', '$stateParams', '$timeout', function ($scope, $filter, $http, editableOptions, editableThemes, projFactory, $stateParams, $timeout) {
            
            var pc = $scope;

            pc.projectSelected = function(){
                            console.log("server pull");
                            pc.project = projFactory.getProjects().get({id:parseInt($stateParams.id,10)})
                                .$promise.then(
                                                    function(response){
                                                        console.log("success");
                                                        pc.project = response;
                                                        console.log(pc.project);
                                                    },
                                                    function(response) {
                                                        console.log("error from project selected");
                                                        pc.message = "Error: "+response.status + " " + response.statusText;
                                                    }
                                    );  
                        return 1;
                        };

            
            //logic check if an ID is in the URL, if so, set it as current project
            if ($stateParams.id >= 0) {
                pc.projectSelected();
            } else {
                console.log("NoProjectSelected");
            }
            
            

            pc.projectFilt = "";

            pc.showProject = false;
            pc.message = "Loading ...";

            
            var delayProjectUpdate = function () {
                console.log("updating");
                pc.projects = projFactory.getProjects().query(
                    function (response) {
                        pc.projects = response;
                        pc.showProject = true;
                    },
                    function (response) {
                        pc.message = "Error: " + response.status + " " + response.statusText;
                    }
                );
                
            };
            
            pc.updateProjects = function () {
                
                $timeout(delayProjectUpdate, 250);
            
            };
            
            pc.initProjects = function () {
                pc.project = {title: "empty", contacts: [], sites: [], mainurl: "", hostingSelect: false, hosting: "", comments: ""};
            };
            
            pc.initProjects();
            pc.updateProjects();
            
            
            pc.hostingOptions = [{value: "cpanel", label: "C-Panel."}, {value: "awsqt", label: "Quantik AWS."}];
            pc.siteTypeOptions = [{value:  "wp", label: "Wordpress"}, {value: "html", label: "Basic HTML"}, {value: "angular",
                                label: "Bootstrap & Angular"}, {value: "node", label: "Node.js"}, {value: "other", label: "Others"}];
            pc.invalidHostingSelection = false;
            
            
            //testings sites
            
            pc.site =  {};
            pc.site.title = "notreSite";
            pc.site.url = "notresite.com";
            pc.site.type = "wordpress";
            pc.site.domains = new Array("fakeDomain");
            pc.site.hosting = {};
            pc.site.hosting.title = "fakeHosting";
            pc.site.hosting.confurl = "www.fakehosting.com/cpanel";
            
            pc.project.sites.push(pc.site);
            
            for (var i = 0 ; i < 5 ; i++){
                pc.site =  {};
                pc.site.title = "notreSite "+i;
                pc.site.url = "notresite"+i+".com";
                pc.site.type = "wordpress"+i;
                
                pc.site.domains = [];
                for (var j = 0 ; j < 3 ; j++){ // add 3 domains to each sites
                    pc.domain = {};
                    pc.domain.url = "www.domain" + j + ".com";
                    pc.domain.redirect = "www.googleNo" + j + ".com";
                    pc.domain.comment = "Comment number " + j;

                    pc.domain.registrar = {};
                    pc.domain.registrar.title = "GoDaddyNumber" + j;
                    pc.domain.registrar.user = "User" + j;
                    pc.domain.registrar.psw = "psw" + j;
                    pc.domain.registrar.panel = "http://cpanel" + j + ".net:3000";
                    pc.domain.registrar.reseller = "reseller" + j;
                    pc.domain.registrar.nameserver = {};
                    pc.domain.registrar.nameserver.ns1 = "ns1.srv" + j +".com";
                    pc.domain.registrar.nameserver.ns2 = "ns2.srv" + j +".com";


                    pc.site.domains.push(pc.domain);
                }
                
                pc.site.hosting = {};
                pc.site.hosting.title = "fakeHosting"+i;
                pc.site.hosting.confurl = "www.fakehosting"+i+".com/cpanel";
                pc.project.sites.push(pc.site);
            }
            
            //wizard on project creation page
            pc.wizardStep =-1;
            pc.field1 = {};
            
            pc.keyUp = function(key){
                if (key.keyCode === 39){
                    pc.wizardSubmit(false);
                    
                }
                else if (key.keyCode === 37){
                    pc.wizardSubmit(true);
                }
            };
            
            pc.wizardSubmit = function(back){
                if (back){
                    pc.wizardStep--;     
                }else{
                    pc.wizardStep++; 
                }
                if (pc.wizardStep == 0){
                    pc.field1.backButton = false;
                }
                else{
                    pc.field1.backButton = true;
                }
                switch (pc.wizardStep){
                    case 0: //name
                        pc.field1.buttonText = "Next or ->";
                        pc.field1.name = "title";
                        pc.field1.placeholder = "Enter title";
                        pc.attrib ="title";
                        break;
                    case 1: //url
                        pc.field1.name = "Main URL";
                        pc.field1.placeholder = "Enter URL";
                        pc.attrib ="mainurl";
                        break;
                    case 2: //hosting
                        pc.field1.name = "hosting";
                        pc.field1.placeholder = "Enter Hosting";
                        pc.attrib ="hosting";
                        break;
                    case 3: //comment
                        pc.field1.name = "comments";
                        pc.field1.placeholder = "Enter Comment";
                        pc.attrib ="comments";
                        pc.field1.buttonText = "Finish or ->";
                        break;
                    case 4: //save
                        pc.sendProject();
                        pc.field1.buttonText = "Next or ->";
                        pc.field1.name = "title";
                        pc.field1.placeholder = "Enter Title";
                        pc.attrib ="title";
                        pc.wizardStep = 0;
                        pc.field1.backButton = false;
                    default:
                        console.log("default switch case");
                } 
                
                console.log(pc.wizardStep);
            };
            pc.wizardSubmit(false);
            
            pc.sendProject = function(){
                console.log("sendProject()");
                if (pc.project.hostingSelect && (pc.project.hosting === "")) {
                    pc.invalidChannelSelection = true;
                    console.log('incorrect hosting selection');
                }
                else {
                    projFactory.getProjects().save(pc.project);
                    console.log(pc.project);
                    pc.updateProjects();
                    pc.invalidChannelSelection = false;
                    pc.initProjects();
                }
                
            };
            
            
                //editable
            editableOptions.theme = 'bs3';
            editableOptions.icon_set = 'font-awesome';
            editableThemes.bs3.inputClass = 'form-control-sm';
            editableThemes.bs3.buttonsClass = 'btn-sm';

            

            


            pc.saveProject = function(data, id) {
                console.log("Updating with id" + id + ". and sending DATA:");
                console.log(data);
                console.log(".");
                projFactory.getProjects().update({id:id},data);
            };
            
            pc.removeProject = function(index) {
                projFactory.getProjects().delete({id:index});
                for (var i = 0 ; i < pc.projects.length ; i++){
                    if (pc.projects[i].id == index){
                        pc.projects.splice(i,1);
                        break;
                    }
                }
            };
            
            pc.addProject = function() {
                //pc.project = {title:"Empty Project",contacts:[], url:"", hostingSelect:false, hosting:"", comments:""};
                pc.initProjects();
                projFactory.getProjects().save(pc.project);
                pc.updateProjects();
            };
            
            
 
            pc.detailLabel = {};
            pc.detailLabel.tableTitle = "TABLE TITLE";
            pc.detailLabel.column1 = "ColumnOne";
  
            pc.editState = {contact:false, site:false, domain:false, email:false, hosting:false, voice:false};
            
            pc.flipActive = function(section) {
                
                console.log("flip active with section :" + section);
                switch (section){
                    case 1: //enable Contact view
                        pc.editState = {contact:true, site:false, domain:false, email:false, hosting:false, voice:false};
                        pc.initContactWizard();
                        break;
                    case 2:
                        pc.editState = {contact:false, site:true, domain:false, email:false, hosting:false, voice:false};
                        pc.initSiteWizard();
                        break;
                    case 3:
                        pc.editState = {contact:false, site:false, domain:true, email:false, hosting:false, voice:false};
                        pc.initDomainWizard();
                        break;
                    case 4:
                        pc.editState = {contact:false, site:false, domain:false, email:true, hosting:false, voice:false};
                        break;
                    case 5:
                        pc.editState = {contact:false, site:false, domain:false, email:false, hosting:true, voice:false};
                        break;
                    case 6:
                        pc.editState = {contact:false, site:false, domain:false, email:false, hosting:false, voice:true};
                        break; 
                    default:
                            pc.editState = {contact:false, site:false, domain:false, email:false, hosting:false, voice:false};
                            console.log("FlipActive default");              
                }
            };
            
            //-----CONTACT SECTION LOGIC START ----//
            pc.saveContact = function(data, id) {
                for (var i = 0 ; i < pc.project.contacts.length ; i++){
                    if (pc.project.contacts[i].$$hashKey == id){
                        pc.project.contacts[i] = data;
                        break;
                    }
                }
                projFactory.getProjects().update({id:pc.project.id},pc.project);
            };
            
            pc.removeContact = function(index) {
                for (var i = 0 ; i < pc.project.contacts.length ; i++){
                    if (pc.project.contacts[i].$$hashKey == index){
                        pc.project.contacts.splice(i,1);
                        break;
                    }
                }
                projFactory.getProjects().update({id:pc.project.id},pc.project);
            };
            
            pc.addContact = function() {
                console.log("addContact");
                pc.contact = {};
                pc.contact.name = "";
                pc.contact.role = "";
                pc.contact.phone = "";
                pc.contact.email = "";
                pc.contact.note = "";
                pc.project.contacts.push(pc.contact);
                projFactory.getProjects().update({id:pc.project.id},pc.project);
            };
            
            pc.initContactWizard = function(){
                pc.wizardStep = -1;
                pc.contactField = {};
                pc.contact = {};
                pc.contactWizardSubmit(false);
            };
            

            pc.contactWizardSubmit = function(back){
                if (back){
                    pc.wizardStep--;     
                }else{
                    pc.wizardStep++; 
                }
                if (pc.wizardStep == 0){
                    pc.contactField.backButton = false;
                }
                else{
                    pc.contactField.backButton = true;
                }
                switch (pc.wizardStep){
                    case 0: //name
                        pc.contactField.buttonText = "Next or ->";
                        pc.contactField.name = "name";
                        pc.contactField.placeholder = "Enter name";
                        pc.attrib ="name";
                        break;
                    case 1: //role
                        pc.contactField.name = "role";
                        pc.contactField.placeholder = "Enter Role";
                        pc.attrib ="role";
                        break;
                    case 2: //phone
                        pc.contactField.name = "phone";
                        pc.contactField.placeholder = "Enter Phone";
                        pc.attrib ="phone";
                        break;
                    case 3: //email
                        pc.contactField.name = "email";
                        pc.contactField.placeholder = "Enter Email";
                        pc.attrib ="email";
                        break;
                    case 4: //note
                        pc.contactField.name = "note";
                        pc.contactField.placeholder = "Enter Note";
                        pc.attrib ="note";
                        pc.contactField.buttonText = "Finish or ->";
                        break;
                    case 5: //save
                        pc.sendContact();
                        pc.contactField.buttonText = "Next or ->";
                        pc.contactField.name = "title";
                        pc.contactField.placeholder = "Enter Title";
                        pc.attrib ="title";
                        pc.wizardStep = 0;
                        pc.contactField.backButton = false;
                    default:
                        console.log("default switch case");
                } 
                console.log("contactWizard" + pc.wizardStep);
            };
            
            pc.sendContact = function(){
                console.log("sendContact()");
                pc.project.contacts.push(pc.contact);
                projFactory.getProjects().save(pc.project);
                pc.contact = {}; 
            };
            //-----CONTACT SECTION LOGIC END ----//
            //-----SITE SECTION LOGIC START ----//
            pc.saveSite = function(data, id) {
                for (var i = 0 ; i < pc.project.sites.length ; i++){
                    console.log("sites hashkey is id ?" + pc.project.sites[i].$$hashKey + " is " + id);
                    if (pc.project.sites[i].$$hashKey == id){
                        pc.site.domains = pc.project.sites[i].domains; //keep those two values
                        pc.site.hosting = pc.project.sites[i].hosting;
                        pc.project.sites[i] = data; //inject the new data from the table
                        pc.project.sites[i].domains = pc.site.domains; //restore those two values
                        pc.project.sites[i].hosting = pc.site.hosting;
                        break;
                    }
                }
                projFactory.getProjects().update({id:pc.project.id},pc.project);
            };
            
            pc.removeSite = function(index) {
                for (var i = 0 ; i < pc.project.sites.length ; i++){
                    if (pc.project.sites[i].$$hashKey == index){
                        pc.project.sites.splice(i,1);
                        break;
                    }
                }
                projFactory.getProjects().update({id:pc.project.id},pc.project);
            };
            
            pc.addSite = function() {
                console.log("addSite log");
                pc.site =  {};
                pc.site.title = "";
                pc.site.url = "";
                pc.site.type = "";
                pc.project.sites.push(pc.site);
                projFactory.getProjects().update({id:pc.project.id},pc.project);
            };
            
            pc.initSiteWizard = function(){
                pc.wizardStep = -1;
                pc.siteField = {};
                pc.site = {};
                pc.siteWizardSubmit(false);
            };
            

            pc.siteWizardSubmit = function(back){
                if (back){
                    pc.wizardStep--;     
                }else{
                    pc.wizardStep++; 
                }
                if (pc.wizardStep == 0){
                    pc.siteField.backButton = false;
                }
                else{
                    pc.siteField.backButton = true;
                }
                switch (pc.wizardStep){
                    case 0: //tile
                        pc.siteField.buttonText = "Next or ->";
                        pc.siteField.name = "title";
                        pc.siteField.placeholder = "Enter tile";
                        pc.attrib ="title";
                        break;
                    case 1: //url
                        pc.siteField.name = "Url";
                        pc.siteField.placeholder = "Enter Url";
                        pc.attrib ="url";
                        break;
                    case 2: //type
                        pc.siteField.name = "type";
                        pc.siteField.placeholder = "Enter type";
                        pc.attrib ="type";
                        break;
                    case 3: //save
                        pc.sendSite();
                        pc.siteField.buttonText = "Next or ->";
                        pc.siteField.name = "title";
                        pc.siteField.placeholder = "Enter Title";
                        pc.attrib ="title";
                        pc.wizardStep = 0;
                        pc.siteField.backButton = false;
                    default:
                        console.log("default switch case");
                } 
                console.log("siteWizard" + pc.wizardStep);
            };
            
            pc.sendSite = function(){
                console.log("sendSite()");
                pc.project.sites.push(pc.site);
                projFactory.getProjects().save(pc.project);
                pc.site = {}; 
            };
            
            pc.displaySiteTypeOptions = function (value) {
                console.log("displaySiteType");
                console.log(value);
                switch (value){
                    case "wp":
                        return "Wordpress";
                        break;
                    case "html":
                        return "Basic HTML";
                        break;
                    case "angular":
                        return "Bootstrap & Angular";
                        break;
                    case "node":
                        return "Node.js";
                        break;
                    case "other":
                        return "Others";
                        break;
                    default:
                        return value;
                        
                }
                return "Wordpress";
            };
            //-----SITE SECTION LOGIC END ----//
          
        }])
    
       
    
    
})();
