(function() {
  'use strict';
  angular
    .module('app')
    .constant("baseURL", "https://qtm4.herokuapp.com/")
    
           .service('projFactory', ['$resource', 'baseURL', function($resource, baseURL) {
                this.getProjects = function(){
                                        return $resource(baseURL+"projects/:id",null,  {'update':{method:'PUT' }});
                                    };
            
                        
        }]);
    
    

})();
